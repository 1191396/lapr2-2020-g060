# UC 9 - Notify Freelancer

## 1. Requirements Engineering

### Brief Format

The administrative starts the process of notifying freelancers. The system asks the administrator if he wants to notify freelancers right now. The administrative confirms. The system notifies freelancers and notifies about the sucess of the operation.

### SSD
![SSD](UC9_SSD.svg)


### Complete Format

#### Main Actor

Administrative

#### Stakeholders and their interests

**Administrative:** intends to inform the freelancers when they are performing below expectations

**Freelancer:** intends to be aware if performing below expectations

**T4J:** intends to inform freelancers that are performing below expectations

#### Preconditions

n/a

#### Postconditions

* An email is sent to all freelancers who have a task mean delay time that is higher than 3 hours and have a percentage of delays that is higher than the overall percentage of delays.

#### Main success scenario (or basic flow)

1. The administrative starts the process of notifying freelancers.
2. The system asks the administrator if he wants to notify freelancers right now.
3. The administrative confirms.
4. The system notifies freelancers and notifies about the sucess of the operation.


#### Extensions (or alternative flows)

*a. There are no applicable freelancers.
> The use case ends.

*a. The administrative cancels the notification process.
> The use case ends.

#### Special requirements

#### List of Technologies and Data Variations
\-

#### Frequency of Occurrence

#### Open Questions

* When the administrative decides to notify freelancers can he also schedule if for a different date?

## 2. OO Analysis

### Excerpt from the Relevant Domain Model for the UC

![MD](UC9_DM.svg)


## 3. Design - Use Case Realization

### Rational

The administrative decides to schedule the notification:

| Main Flow | Question: Which Class... | Answer | Justification |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. The administrative starts the process of notifying freelancers. | ...interacts with the administrative? | NotifyFreelancerUI | Pure Fabrication |
|                                                      | ...coordinates the use case? | NotifyFreelancerController | Controller |
| 2. The system asks the administrator if he wants to notify freelancers right now. |   |   |   |
| 3. The administrative confirms. | ...knows the freelancers that apply? | NotifyFreelancer | IE: NotifyFreelancer has a list of freelancers that are applicable. |
| 4. The system notifies freelancers and notifies about the sucess of the operation. | ...knows the email api to be used? | Platform | IE: in the DM Platform uses SendEmail |
|                                                     | ...knows each freelancer email? | RegisterFreelancer | IE: RegisterFreelancer has all freelancers registered. |
|                           | ...gets the email api to be used? | SendEmail | IE: in the DM SendEmail gets the email API |
|                           | ...sends an email to Freelancers? | Any class instanciated by SendEmail | Protected Variation |

### Systematization ##

 It follows from the rational that the conceptual classes promoted to software classes are:

 * Platform
 * Freelancer
 * NotifyFreelancer
 * SendEmail

Other software classes (i.e. Pure Fabrication) identified:

 * NotifyFreelancersUI
 * NotifyFreelancersController
 * RegisterFreelancer
 * RegisterOrganization

###	Sequence Diagram

![CD.svg](UC9_SD.svg)

SD_getFreelancersThatApply()
![CD.svg](UC9_SD_getFreelancersThatApply().svg)

SD_notifyFreelancers()
![CD.svg](UC9_SD_notifyFreelancers().svg)

###	Class Diagram

![CD.svg](UC9_CD.svg)
