# UC 9 - Notify Freelancer

## 1. Requirements Engineering

### Brief Format

The Timer starts the freelancer notification process.

### SSD
![SSD](UC9_SSD.svg)


### Complete Format

#### Main Actor

Timer

#### Stakeholders and their interests

**Freelancer:** intends to be aware if performing below expectations

**T4J:** intends to inform freelancers that are performing below expectations

#### Preconditions

n/a

#### Postconditions

* An email is sent to all freelancers who have a task mean delay time that is higher than 3 hours and have a percentage of delays that is higher than the overall percentage of delays.

#### Main success scenario (or basic flow)

1. The Timer starts the freelancer notification process.


#### Extensions (or alternative flows)

*a. There are no applicable freelancers.
> The use case ends.


#### Special requirements

#### List of Technologies and Data Variations
\-

#### Frequency of Occurrence
Periodically, with the scheduling done by the system, more specifically, the last day of every year.

#### Open Questions

* Is there a specific time of day when the emails should be forwarded?

## 2. OO Analysis

### Excerpt from the Relevant Domain Model for the UC

![MD](UC9_MD.svg)


## 3. Design - Use Case Realization

### Rational

| Main Flow | Question: Which Class... | Answer | Justification |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. The Timer starts the freelancer notification process. | ...forwards the emails? | RegisterFreelancer |  |
|                                                     | ...knows the freelancers that apply? | NotifyFreelancerTask | IE: NotifyFreelancer has a list of freelancers that are applicable. |
|                                                     | ...knows each freelancer email? | RegisterFreelancer | IE: RegisterFreelancer has all freelancers registered. |
|                           | ...gets the email api to be used? | SendEmail | IE: in the DM SendEmail gets the email API |
|                           | ...sends an email to Freelancers? | Any class instanciated by SendEmail | Protected Variation |
|                           | ...schedules the notification? | Platform | IE |
|                           | ...runs the scheduled task? | NotifyFreelancerTask | NotifyFreelancerTask is a TimerTask |

### Systematization ##

 It follows from the rational that the conceptual classes promoted to software classes are:

 * Platform
 * NotifyFreelancer
 * Freelancer

Other software classes (i.e. Pure Fabrication) identified:

 * NotifyFreelancerTask
 * RegisterFreelancer
 * RegisterOrganization

###	Sequence Diagram

![SD.svg](UC9_SD.svg)

SD_getFreelancersThatApply()
![CD.svg](UC9_SD_getFreelancersThatApply().svg)

SD_notifyFreelancers()
![CD.svg](UC9_SD_notifyFreelancer.svg)

###	Class Diagram

![CD.svg](UC9_CD.svg)
