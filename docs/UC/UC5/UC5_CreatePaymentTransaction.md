
# UC5 - Criar Transação de Pagamento

## 1. Engenharia de Requisitos

### Formato Breve

O Colaborador inicia a criação de uma nova transação de pagamento. O sistema solicita a escolha da tarefa duma lista de tarefas concluidas. O Colaborador identifica a tarefa qual pretende selecionar para a transação. O sistema apresenta os dados do Freelancer que a executou e pede confirmação. O Colaborador confirma os dados. O sistema solicita a introdução dos detalhes da execução da tarefa (data de conclusão, delay da execução, descrição breve da qualidade do trabalho). O Colaborador introduz os dados solicitados. O sistema valida os dados e solicita confirmação. O Colaborador confirma. O sistema apresenta o valor a pagar pela tarefa, pedindo ao Colaborador que o confirme. O Colaborador confirma. O sistema **cria a transação do pagamento e guarda esse no plataforma** e informa o Colaborador do sucesso da operação.


### SSD

![UC5_SSD.svg](UC5_SSD.svg)


### Formato Completo

#### Ator principal

Colaborador da Organizacao

#### Partes interessadas e seus interesses

* **Colaborador:** pretende criar transações de pagamentos para que estes possam ser guardados no histórico do registo de transações e serem descarregados posteriormente.
* **Freelancer:** pretende receber pagamentos pelas tarefas executadas.
* **Organizacao:** pretende que os Freelancers possam ser remunerados pelas tarefas executadas.
* **T4J:** pretende que seja possível para a Organizacao em causa efetuar pagamentos aos Freelancers.


#### Pré-condições

* A tarefa pretendida para a transação deve ser criada e possuir os respetivos dados.
* Todas as tarefas na lista devem ser concluidas.
* O Freelancer que concluiu a tarefa deve ser registado na plataforma e possuir todos os dados obrigatórios.
* O valor de pagamento deve ser capaz de ser automaticamente calculado para poder ser gerado no ato da criação da transação.

#### Pós-condições

A transação do pagamento é criada e guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)
1. Collaborator starts a new transaction
2. The system prompts you to choose the task from a list of completed tasks
3. The cloborator selects a traf
4. The sisma shows a list of freelacers and prompts you to select 1.
5. The employee selects the freelacer
6. The system asks for data (due date, short description)
7. The collaborator enters the requested data
8. The system validates the data and asks for confirmation.
9. The Collaborator confirms
10. informs about the success of the operation and shows the amount to be paid.

#### Extensões (ou fluxos alternativos)

*a. O Colaborador solicita o cancelamento da criação da transação.

> O caso de uso termina.

3a. A tarefa pretendida não se encontra na lista apresentada.
>   1. O sistema apresenta a opção da criação de uma nova tarefa.
>   2. O colaborador seleciona essa opção e é encaminhado para o caso de uso CriarTarefa. O caso de uso termina.

4a. O Freelancer responsável pela execução da tarefa não está presente ou possui dados errados.
>   1. O sistema apresenta a opção de registo de um novo Freelancer.
>   2. O colaborador seleciona essa opção e é encaminhado para o caso de uso RegisterFreelancer. O caso de uso termina.

8a. Dados mínimos obrigatórios em falta.
>   1. O sistema informa quais os dados em falta.
>   2. O sistema permite a introdução dos dados em falta (passo 7).
>
	>   2a. O Colaborador não altera os dados. O caso de uso termina.

8b. O sistema deteta que os dados introduzidos (ou algum subconjunto de dados) são inválidos.
>   1. O sistema alerta o Colaborador para o facto.
>   2. O sistema permite a sua alteração (passo 7).
>
    >   2a. O Colaborador não altera os dados. O caso de uso termina.


#### Requisitos especiais

* O valor de pagamento é apresentado em euros e também em moeda do país do Freelancer.

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Todos os detalhes da execução da tarefa são obrigatórios?
* O que acontece caso o valor a pagar esteja inválido?
* Existem outros dados obrigatórios para além dos já conhecidos?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC5_MD.svg](UC5_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. The Collaborarot  starts the creation of a new payment transaction.  |	... interage com o utilizador? | CreatePaymenteOrderUI |  Pure Fabrication |
|  		 |	... coordena the UC?	| CreatePaymenteOderController | Controller    |
|  		 |	... create Transaction instances? | RegisterPaymanetOrder | Creator (Regra1) + HC/LC  |
||...do you know the user / manager using the system?|UserSession|IE: cf. .|
||...|RegisterOrganization|IE:knows all organizations.|
|||Organization|IE: know your collaborators
.|
|||Colabdo you know which organization the user / employee belongs to?|IE:knows your data (e.g. email). |
| 2. The system prompts you to choose the task from a list of completed tasks. 	 |...knows the tasks? |Organization|IE: no MD the Organization has Tasks. |
| | |TaskList|IE: in the MD the Organization has Tasks. By application of HC + LC delegates the TaskList|
| 3. The Colaborator identifies the desired task
. |   Task | Information Expert (IE)-instance created in step 1: has its own data.     |
| 4. The system displays the data of the Freelancer who performed it and asks for confirmation. | ... do you know the list of categories? | RegisterCategories | IE: RegistoCategories has all instances of Category. |
|5. The Collaborator confirms. | ... save the selected category? | Task | IE: it has its own data - in the MD the Task falls into a Category. |
| 6. The system prompts you to enter the details of the task execution (completion date, delay of execution, brief description of the quality of work). | ... validates the Task data (local validation)? | Task | IE: has its own data.
| | ... validates the Task data (global validation)? | TransactionList | IE: the TransactionList contains / aggregates Task. |
| 7. The Collaborator enters the requested data. | | | |
| 8. The system validates the data and asks for confirmation. | ... keep the created Task? | TransactionList | IE: the TransactionList contains / aggregates Task. |
| 9. The Collaborator confirms.
| 12. The system presents the data creates the payment transaction and stores it on the platform and informs the Employee of the success of the operation.|


### Sistematização ###

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * Colaborador
 * Freelancer
 * Tarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:

 * CreateTransactionUI
 * CreateTransactionController


###	Diagrama de Sequência

![UC5_SD.svg](UC5_SD.svg)



###	Diagrama de Classes

![UC5_CD.svg](UC5_CD.svg)

