# UC 4 - Create Freelancer

## 1. Requirements Engineering

### Brief Format
The collaborator starts registering a freelancer.
The system requests the data that characterizes the freelancer (i.e. name, level of expertise, e-mail, NIF, bank account (IBAN), address, and country).
The collaborator enters the requested data.
The system validates and displays the data, asking the collaborator to confirm it.
The collaborator confirms.
The system registers the freelancer's data and informs the collaborator of the success of the operation.

### SSD
![UC4_SSD](UC4_SSD.svg)


### Complete Format

#### Main Actor

Collaborator

#### Stakeholders and their interests

**Collaborator:** intends to register the freelancer in the system so that the freelancer can be paid.

**Freelancer:** intends to be registered to be able to receive payments.

**T4J:** intends the freelancer to be registered in the system to be able to pay the freelancer.

#### Preconditions

n/a

#### Postconditions

* The freelancer registration information is saved in the system.

#### Main success scenario (or basic flow)

1. The collaborator starts registering a freelancer.
2. The system requests the data that characterizes the freelancer (i.e. name, level of expertise, e-mail, NIF, bank account (IBAN), address, and country).
3. The collaborator enters the requested data.
4. The system validates and displays the data, asking the collaborator to confirm it.
5. The collaborator confirms.
6. The system registers the freelancer's data and informs the collaborator of the success of the operation.


#### Extensions (or alternative flows)

*a. The collaborator requests the cancellation of the registration.

> The use case ends.

2a. Address data incomplete.
>	1. The system informs which data is missing.
>	2. The system allows the collaborator to enter the missing data.
>
	>	2a. The collaborator does not change the data. The use case ends.
	
2b. Missing minimum required data.
>	1. The system informs which data is missing.
>	2. The system allows the collaborator to enter the missing data.
>
	>	2a. The collaborator does not change the data. The use case ends.

2c. The system detects that the data (or some subset of the data) entered ** (e.g. email) ** must be unique and that it already exists in the system.
>	1. The system informs the collaborator.
>	2. The system allows the collaborator to change the data.
>
	>	2a. The collaborator does not change the data. The use case ends.


#### Special requirements

#### List of Technologies and Data Variations
\-

#### Frequency of Occurrence
\-

#### Open Questions

* Are there any other mandatory data in addition to those already known?
* What data together can detect the duplication of freelancers?
* How often does this use case occur?

## 2. OO Analysis

### Excerpt from the Relevant Domain Model for the UC

![UC4_DM](UC4_DM.svg)


## 3. Design - Use Case Realization

### Rational

| Main Flow | Question: Which Class... | Answer | Justification |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. The collaborator starts registering a freelancer. | ...interacts with the collaborator? | CreateFreelancerUI | Pure Fabrication |
|                                                      | ...coordinates the use case? | CreateFreelancerController | Controller |
|                                                     | ...creates an instance of Freelancer? | RegisterFreelancer | HC/Creator (Rule1) |
|   | ...generates the id for the freelancer? | RegisterFreelancer | HC/IE: in the DM RegisterFreelancer has and therefore knows all Freelancers registered. So it can count how many freelancers exist with the same initials in the system. |
| 2. The system requests the data that characterizes the freelancer (i.e. id, name, level of expertise, e-mail, NIF, bank account (IBAN), address, and country). | | | |
| 3. The collaborator enters the requested data. | ...saves the entered data? | Freelancer, Address | IE: instance created in step 1 |
|                                                | ...creates an instance of Address? | Freelancer | Creater (rule1)
|                                                |                                    |            | In the domain model Freelancer has Address |
| 4. The system validates and displays the data, asking the collaborator to confirm it. |...validates the Freelancer data? (local validation)|Freelancer| IE: has its own data |
|                                                                                       | ...validates the Address data? (local validation) | Address | IE: has its own data |
|                                                                        | ...validates the Address data? (global validation) | Freelancer | IE: Freelancer has its own Adress |
|                                      | ...validates the Freelancer data? (global validation) | RegisterFreelancer | IE: RegisterFreelancer has Freelancer registered |
| 5. The collaborator confirms. | | | |
| 6. The system registers the freelancer's data and informs the collaborator of the success of the operation. | ...saves the Freelancer created? | RegisterFreelancer | IE/HC: RegisterFreelancer has Freelancer

### Sistematization ##

 It follows from the rational that the conceptual classes promoted to software classes are:

 * Platform
 * Freelancer
 * Address
 * Organization

Other software classes (i.e. Pure Fabrication) identified:

 * CreateFreelancerUI
 * CreateFreelancerController
 * RegisterFreelancer
 * RegisterOrganization

Other classes of external systems / components:

 * UserSession

###	Sequence Diagram

![UC4_SD](UC4_SD.svg)

![UC4_SD_setId(name)](UC4_setIdName.svg)


###	Class Diagram

![UC4_CD.svg](UC4_CD.svg)
