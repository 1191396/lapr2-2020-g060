# UC2 - Load transaction

## 1. Requirements Engineering


### Brief Format

The employee of the organization starts to upload a new file with a set (historical) of financial transactions. The system asks the employee to indicate the directory and file to be loaded. The employee enters the requested information. The system displays the data entered and asks for confirmation. The employee confirms. The system loads the data from the file, validates and informs the success of the operation.


### SSD
! [UC2-SSD] (UC2_SSD.svg)


### Complete Format

#### Main actor

collaborator

#### Stakeholders and their interests


* **Collaborator:** intends to upload data from a file.
* **Organization:** intends to upload transaction data.

#### Preconditions

The file must:

* be of the type ".txt" or ".csv";
* follow the standard layout;
* and the platform must have access to the indicated folder.


#### Postconditions

Financial transaction data is loaded into the system.

#### Main success scenario (or basic flow)



1. The employee of the organization starts uploading a new file with a set (history) of financial transactions.
2. The system asks the employee to indicate the directory and file to be loaded.
3. The employee enters the requested information.
4. The system displays the data entered and asks for confirmation.
5. The collaborator confirms that the directory and file name are correct.
6. The system loads the data from the file, validates and reports the success of the operation.


#### Extensions (or alternative flows)

*The. The employee requests the cancellation of the operation.

> The use case ends.

6. The file indicated is not a valid text file (.csv or .txt).
> 1. The system indicates that the selected file is not valid.
> 2. The system allows the employee to indicate a valid file. (step 2)
>
    > 2a. The employee does not change the data. The use case ends.
    > 2b. The employee indicates a new invalid file. The use case ends.

6. The file contains invalid information and does not allow objects to be correctly instantiated.
> 1. The system does not register objects in the line with errors.
> 2. The system creates an error report in the same directory where the file was loaded.
> 3. The system proceeds to read the next line.



#### Special requirements
(list special requirements applicable only to this UC)

\ -

#### List of Technologies and Data Variations
It must support the loading of different types of files (txt and csv).
\ -

#### Frequency of Occurrence
(describe how often this UC is held)

\ -

#### Open questions

(list of open questions, i.e. without a known answer.)

## 2. OO Analysis

### Excerpt from the Relevant Domain Model for UC

! [UC2-MD] (UC2_MD.svg)



## 3. Design - Use Case Realization

### Rational

| Main Flow | Question: Which Class... | Answer | Justification |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. The employee of the organization starts uploading a new file with a set (history) of financial transactions. | ... interact with the employee? | LoadTransactionUI | Pure Fabrication |
| | ... coordinates the UC? | LoadFilleController | Controller |
| | ... creates LoadFiller instance? | LoadTransactionRegister | Creator (Rule1) |
| | ... knows a user using the system? | SessaoUser | IE: user management component. |
| | ... knows which organization the user belongs to? | RegisterOrganization | IE: knows all organizations. |
| | Organization | IE: knows all employees. |
| | Collaborattor | IE: know your data (email). |
| 2. The system asks the employee to indicate the directory and file to be loaded. |
| 3. The employee enters the requested information. | ... keep inserted information? | LoadTransaction | IE: has / knows information about the file. |
| 4. The system displays the data entered and asks for confirmation. || |
| 5. The collaborator confirms that the directory and file name are correct. | ... validates the file? (local validation) | LoadTransaction | IE: you have / know your data. |
|| ... create payment orders? | PaymentTransactionRegister | Creator (Rule1) + HC / LC: on MD Platform you have money orders. By HC / LC delegates these responsibilities to PaymentTransactionRegister. |
|| ... validates money orders? (local validation) | PaymentTransaction | IE: you have / know your data.
|| ... validates money orders? (global validation) | PaymentTransactionRegister | IE: contains / aggregates PaymentTransaction |
| 6. The system loads the data from the file, validates and reports the success of the operation. | | | |


### Sistematização ##

 It follows from the rational that the conceptual classes promoted to software classes are:

 * SessaoUtilizador
 * LoadTransaction
 * Transaction
 * Freelancer
 * Task

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * LoadTransactionUI  
 * LoadTransactionController
 * RegisterLoadTransaction
 * RegisterPaymentTransaction
 * RegisterFreelancer
 * RegisterTask


###	Diagrama de Sequência

![SD_UC2.svg](UC2_SD.svg)


###	Diagrama de Classes

![CD_UC2.svg](UC2_CD.svg)
