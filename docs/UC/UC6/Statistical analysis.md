# UC6_ statistical analysis

# UC3 - Check Performance Indicators

## 1. Requirements Engineering

### Brief Format

The Colaborator/Manager of the platform requires to check the performance indicators (Payments, delays & normal distribution > 3 hours). The system shows the indicators of performance in form of histograms and list 
* The view of the charts and its data is managed by the manager/administrator through the filters that was disponibilized by the platform.

### SSD
![SSD6.jpg](SSD6.jpg)



### Complete Format

#### Main Actor

Manager/colaborator

#### Stakeholders and their interests

* **T4J:** Wants to know the performance of its freelancers.


#### Preconditions

- Most have a pre existent organization and payment transactions and the task.

#### Postconditions
\-

### Main success scenario (or basic flow)

1. The Manager requires to check the indicators of performance of the freelancers
2. The system shows the results.

3. **The MAnger/Colaborato may or may not change filters**
4. **The system will update the histograms and lists based on the filters**



#### Extensions (or alternative flows)

*a. The manager/Collaborator requests cancellation of the operation


> the use case ends.


#### Special requirements
\-
#### List of Technologies and Data Variations
\-

#### Frequency of Occurrence
\-

#### Open Questions

\-

## 2. OO Analysis

### Excerpt from the Relevant Domain Model for the UC

![OO6.jpg](OO6.jpg)

## 3. Design - Use Case Realization

### Rational
|Main flux | Q: Wich class... | Answer  | Justification  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1.The Colaborator/Manager of the platform requires to check the performance indicators |...interacts with user?	| PerformaceIndicatorUI | Pure Fabrication | 
|     |...coordinate the UC?|PerformaceIndicatorController|Controller|
|The system shows the results.||||



### Sistematization ##


 From rational, conceptual classes that was promoted to software classes are:

 *
 *
 *
 *


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 *
 *
 *
 *

Outras classes de sistemas/componentes externos:

\-

###	Sequence Diagram

![SD.jpg](SD.jpg)

###	Class Diagram
![CD.svg](UD.svg)


