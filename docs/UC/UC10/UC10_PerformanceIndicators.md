# UC3 - Check Performance Indicators

## 1. Requirements Engineering

### Brief Format

The administrator of the platform requires to check the performance indicators (Payments, delays & normal distribution > 3 hours). The system shows the indicators of performance in form of histograms and list (in case of normal distribution > 3 hours).
* The view of the charts and its data is managed by the administrator through the filters that was disponibilized by the platform.

### SSD
![UC10_SSD.svg](UC10_SSD.svg)


### Complete Format

#### Main Actor

Administrator

#### Stakeholders and their interests

* **T4J:** Wants to know the performance of its freelancers.


#### Preconditions

- Most have a pre existent organization and payment transactions.

#### Postconditions
\-

### Main success scenario (or basic flow)

1. The administrator requires to check the indicators of performance of the freelancers
2. The system shows the results (Payments, delays & normal distribution > 3 hours).
3. **The administrator may or may not change filters**
4. **The system will update the histograms and lists based on the filters**



#### Extensions (or alternative flows)

*a. O colaborador de organização solicita o cancelamento da especificação de tarefa.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o colaborador de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o colaborador de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O colaborador de organização não altera os dados. O caso de uso termina.


#### Special requirements
\-
#### List of Technologies and Data Variations
\-

#### Frequency of Occurrence
\-

#### Open Questions

\-

## 2. OO Analysis

### Excerpt from the Relevant Domain Model for the UC

![UC10_MD.svg](UC10_MD.svg)

## 3. Design - Use Case Realization

### Rational
|Main flux | Q: Wich class... | Answer  | Justification  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. ||  |  |



### Sistematization ##


 From rational, conceptual classes that was promoted to software classes are:

 *
 *
 *
 *


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 *
 *
 *
 *

Outras classes de sistemas/componentes externos:

\-

###	Sequence Diagram

![UC10_SD.svg](UC10_SD.svg)

###	Class Diagram

![UC10_CD.svg](UC10_CD.svg)
