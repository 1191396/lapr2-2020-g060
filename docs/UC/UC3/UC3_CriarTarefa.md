# UC3 - Create Task

## 1. Requirements Engineering

### Brief Format

The collaborator of the organization starts the creation of a task. The system requests the necessary data (e.g. ID, brief description, a time duration, cost and a task category).
The system **validates** and displays the data, asking the collaborator to confirm it.
The collaborator confirms.
The system **registers the data** and informs the collaborator of the success of the operation.

### SSD
![UC3_SSD.svg](UC3_SSD.svg)


### Complete Format

#### Main Actor

Collaborator

#### Stakeholders and their interests

* **Collaborator** Need to create tasks.
* **Organização:** Want that theirs collaborators can create tasks to further publication.
* **Freelancer:** Want to canidate to new tasks
* **T4J:** Wants Freelancers to be executing tasks in their platform.


#### Preconditions

n/a

#### Postconditions
* The information of the new task is registered in the System.

### Main success scenario (or basic flow)

1. The collaborator of the organization starts the creation of a new task
2. The System requests the necessary data (e.g. ID, brief description, a time duration, cost and a task category).
3. The Collaborator of the organization enters the requested data.
4. The System validates and presents the data to the collaborator of the organization, asking for confirmation
5. The collaborator confirms
6. The System register the datas and informs the collabotator about the sucess of the opration.

#### Extensions (or alternative flows)

*a. The organization employee requests to cancel the task specification.

> The use case ends.

4a. Missing minimum required data.
>	1. The system informs you which data is missing.
>	2. The system allows entry of missing data (step 3)
>
	>	2a. The organization employee does not change the data. The use case ends.

4b. The system detects that the data (or some subset of the data) entered must be unique and that it already exists in the system.
>	1. The system alerts the organization employee to the fact.
>	2. The system allows you to change it (step 3)
>
	>	2a. The organization employee does not change the data. The use case ends.

4c. The system detects that the entered data (or some subset of the data) is invalid.
> 1. The system alerts the organization employee to the fact.
> 2. The system allows you to change it (step 3)
>
	> 2a. The organization employee does not change the data. The use case ends.


#### Special requirements
- The estimated duration of a task is indicated in hours.
- The estimated cost is indicated in euros.
#### List of Technologies and Data Variations
\-

#### Frequency of Occurrence
\-

#### Open Questions

\-

## 2. OO Analysis

### Excerpt from the Relevant Domain Model for the UC

![UC3_MD.svg](UC3_MD.svg)

## 3. Design - Use Case Realization

### Rational
| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. The organization employee starts creating a task. | ... interact with the user? | CreateTaskUI | Pure Fabrication: there is no justification for assigning this responsibility to any existing class in the Domain Model. |
| | ... coordinates the UC? | Create TaskController | Controller |
| | ... creates a Task instance? | TaskList | Creator (Rule1) + HC / LC: in MD the Organization has a Task. By HC / LC delegates these responsibilities in TaskList. |
|| ... do you know the user / manager using the system? | UserSession | IE: cf. user management component documentation.
|| ... do you know which organization the user / collaborator belongs to? | Organizational Register | IE: knows all organizations. |
||| Organization | IE: know your employees. |
||| Collaborator | IE: knows your data (e.g. email). |
| 2. The system asks for the necessary data (i.e. an ID, a short designation, an estimate of duration and cost, the task category). | | | |
| 3. The organization employee enters the requested data. | ... keep the data entered? | Task | Information Expert (IE) -instance created in step 1: it has its own data. |
| 4. The system validates and presents the data to the organization employee, asking him to confirm it. | ... validates the Task data (local validation)? | Task | IE: has its own data.
| | ... validates the Task data (global validation)? | TaskList | IE: the TaskList contains / aggregates Task. |
| 5. The organization employee confirms the data. | | | |
| 6. The system records the data and informs the organization employee of the success of the operation. | ... keep the created Task? | TaskList | IE: the TaskList contains / aggregates Task. |

### Sistematization ##

From the rational the conceptual classes that was promoted to software classes are:

 * Organization
 * Platform
 * Assignment



Other software classes (i.e. Pure Fabrication) identified:

 * CreateTasksUI
 * CreateTaskController
 * TaskList
 * RegistrationOrganizations


Other classes of external systems / components:

 * UserSession

###	Sequence Diagram

![UC3_SD.svg](UC3_SD.svg)

###	Class Diagram

![UC3_CD.svg](UC3_CD.svg)
