# UC 7 - Set day of payment

## 1. Requirements Engineering


### Short Format
The manager starts set a day of payment transaction. The system show payments transactions that need to be set a day to execution from managers company. The manager choose a payment transaction. The system request date of execution. The manager set a date of payament. The system validate and request confirmation. The manager confirm. The system register date, inform the success of operation.

### SSD
! [UC7-SSD] (UC7_SSD.svg)


### Full Format

#### Main actor

Manager

#### Stakeholders and their interests

* ** Manager: ** intends to schedule the execution of a payment order.
* ** Freelancer: ** intends to receive for the work delivered.
* ** T4J: ** intends for transactions to be successfully completed.

#### Preconditions

There is a company payment order to be scheduled.

#### Postconditions

Creates a timer to execute the payment order.

#### Main success scenario (or basic flow)

1. The manager starts set a day of payment transaction.
2. The system show current default organization's day of payment, and request a new date.
3. The manager set a day of payment.
4. The system validate and request confirmation.
5. The manager confirm.
6. The system register date, inform the success of operation.


#### Extensions (or alternative flows)

*The. The manager request cancel of operation.

> The use case ends.

2. There are no payment orders created for the organization.
> 1. The system informs the user (the use case ends).

6. The day choosed is not valid.
> 1. The system informs the manager that the payment date is not valid.
> 2. The system allows a new date to be entered.
>
    > 2a. The manager does not change the data. The use case ends.


#### Special requirements

\ -

#### List of Technologies and Data Variations

\ -

#### Frequency of Occurrence


\ -

#### Open questions


## 2. OO Analysis

### Excerpt from the Relevant Domain Model for UC

(Present here an excerpt from the relevant MD for this UC)


## 3. Design - Use Case Realization

### Rational

| Main Flow | Question: What Class ... | Answer | Justification |
|: -------------- |: ---------------------- |: -------- - |: ---------------------------- |
| 1. The manager starts set a day of payment transaction. | ... interacts with user? | PaymentDayUI | Pure Fabrication |
|| ... coordinate the UC? | PaymentDayController | Controller |
|| ... knows the user using the system? | UserSection |
|| ... knows Manager's Organization? | Platform | At DM Platform knows all organizations.
||| RegisterOrganization | IE: apply HC + LC, Platform delegate to RegisterOrganization. |
| 2. The system show current default organization's day of payment, and request a new date. | ... knows the payment date? | Organization | IE: has a payment day. |
| 3. The manager set a day of payment. | | | |
| 4. The system validate and request confirmation. | ... validate data? (local validation) | Organization | IE: has their own data. |
| 5. The manager confirm. | | | |
| 6. The system register date, inform the success of operation. | | | |

### Systematization ##

 It follows from the rational that the conceptual classes promoted to software classes are:

 * Organization

Other software classes (i.e. Pure Fabrication) identified:

 * SetPaymentDayUI
 * SetPaymentDayController
 * RegisterOrganization


### Sequence Diagram

! [UC7_SD.svg] (UC7_SD.svg)


### Class Diagram

! [CD_UC7.svg] (UC7_CD.svg)
