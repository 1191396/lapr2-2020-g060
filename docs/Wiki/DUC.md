# Diagrama de Casos de Uso

![DUC](DUC.svg)




# Casos de Uso
| UC  | Descrição                                                               |                   
|:----|:------------------------------------------------------------------------|
| UC1 | [Register new Organization](UC5_registernNewOrganisation.md)   |
| UC2 | [Create a task]()  |
| UC3 | [Specify Freelancer]()|
| UC4 | [Schedule a payment transation order]()|
| UC5 | [add new  organization](UC5_addneworganization.md) |
| UC6 | [Sypecify manager of the organisation](UC6_Sypecifymanageroftheorganisation.md)|
| UC7 | []()|
| UC8 | [Excecute a payment order]()|
| UC9 | [Notify freelancerdelay](UC5_Notifyfreelancerdelay.md) |
| UC10 | [Load historical transaction informatin](UC6_Loadhistoricaltransactioninformatin.md)|