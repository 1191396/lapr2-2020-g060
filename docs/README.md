# Laboratório de Projeto 2019-2020


# 1. Constituição do Grupo de Trabalho ###

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	   | Nome do Aluno			     |
|--------------|-----------------------------|
| **1171351**  | Arcenio Uate                |
| **1192220**  | Ekaterina Aksenova          |
| **1191396**  | Marcel Gavioli              |
| **1182115**  | Matheus Aguiar              |
| **1191573**  | Rita Cordeiro               |

# 2. Wiki

* [Enunciado](Wiki/Enunciado.md)
* [Glossario](wiki/Glossario.md)
* [Casos de Uso](wiki/DUC.md)
* [Especificação Suplementar](wiki/FURPS.md)
* [Modelo de Dominio](wiki/MD.md)


# 3. Distribuição de Tarefas ###

A distribuição de tarefas ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.


| Tarefa                      | [Sprint 1] | [Sprint 2] | |
|-----------------------------|------------|------------|------------|
| Diagrama Casos de Uso (DUC) |  [todos](Wiki/DUC.md)   |     -     |     -     |
| Glossário  |  [todos](Wiki/Glossario.md)   |     -     |     -     |
| Especificação Suplementar   |  [todos](Wiki/FURPS.md)     |     -     |     -     |
| Modelo de Domínio           |  [todos](Wiki/MD.md)   |     -     |     -     |
| UC 1 (Register Organization)  |  [1171351](UC/UC1/UC1_RegistarOrganizacao.md)   |     -     |     -     |
| UC 2 (Load Transaction)  |  [1191396](UC/UC2/UC2_LoadTransaction.md)   |     -     |     -     |
| UC 3 (Create Task)  |  [1182115](UC/UC3/UC3_CriarTarefa.md) |     -     |     -     |
| UC 4 (Create Freelancer)  |  [1191573](UC/UC4/UC4_EspecificarFreelancer.md)   |     -     |     -     |
| UC 5 (Create Payment Transaction)  |  [1192220](UC/UC5/UC5_CriarOrdemPagamento.md)   |     -     |     -     |
| UC 6 (Analyse Freelancer Statistics)  |    -     | [1171351](UC/UC6/UC6_AnalyseFreelancerStatistic.md)     |     -     |
| UC 7 (Set day of payment)  |    -     | [1191396](UC/UC7/UC7_SetDayofPayment.md)  |     -     |
| UC 8 (Make payments (automatically))  |    -     | [1192220](UC/UC8/UC8_MakePayments.md)     |     -     |
| UC 9 (Notify Freelancer(automatically))  |    -     | [1191573](UC9_NotifyFreelancer.md)     |     -     |
| UC 10 (Check performance indicators) |    -     | [1182115](UC/UC10/CheckPerformancerIndicators.md)  |     -     |




